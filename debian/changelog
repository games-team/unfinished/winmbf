winmbf (2.03+svn18+dfsg1-1) unstable; urgency=low

  * Initial release.
  * Remove internal data of spurious origin, e.g. DOGS graphics and sounds,
    BETA graphics, additional menu graphics, aid for ancient Doom versions.
  * Apply a series of patches:
    + Improve portability by providing Autoconf/Automake helper files and
      fixing build issues on Linux/GCC/Glibc.
    + Fix format string vulnerabilities.
    + Fix a segfault when a menu is left while not in-game.
    + Fix another segfault with empty reject matrices,
      as e.g. found in the HACX 1.2 IWAD.
    + Fix another segfault with single subsector maps.
    + Fix another segfault when a video mode fails to set.
    + Add support for a global data directory.
    + Change location for config file and tranmap.dat to a directory
      beneath $HOME, extend to screenshots and save games as well.
    + Add support for Freedoom and the Doom Classic data supplied with the
      Doom 3 BFG Edition.
    + Replace missing menu entries and titles by text written in the HU font.

 -- Fabian Greffrath <fabian@debian.org>  Tue, 09 Feb 2016 21:35:42 +0100
